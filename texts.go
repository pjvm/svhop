/* Copyright 2023 PJ van Mill (a.k.a. pjvm) and the svhop authors, who are
 * indicated by the AUTHORS file in the svhop source repository
 *
 * License: BSD-2-Clause */

package main

var texts textset = textset{
	msgOver1026: "59 request too long (reading max. 1026 bytes)\r\n",
	msgParseError: "59 could not parse url\r\n",
	msgPossiblyMisconfigured: "50 server may be misconfigured\r\n",
	msgClientTimeout: "40 timed out waiting for your request\r\n",

	errprefixSetenv: programname + ": could not set environment variable: ",
	errprefixChainload: programname + ": could not chainload: ",
}
