/* Copyright 2023 PJ van Mill (a.k.a. pjvm) and the svhop authors, who are
 * indicated by the AUTHORS file in the svhop source repository
 *
 * License: BSD-2-Clause */

package main

import (
	"syscall"
	"time"
	"bufio"
	"net/url"
	"io"
	"os"
	"strings"
	"fmt"
	"os/exec"
)

const programname string = "svhop-parsehost"

func main() {
	go reap()

	in := io.LimitReader(os.Stdin, 1026)
	r := bufio.NewReader(in)
	req, err := r.ReadString('\n')
	if err != nil {
		io.WriteString(os.Stdout, texts.msgOver1026)
		os.Exit(2)
	}
	urlend := strings.IndexAny(req, " \r\n")
	urlstring := req[:urlend]
	parsed, err := url.Parse(urlstring)
	if err != nil {
		io.WriteString(os.Stdout, texts.msgParseError)
		os.Exit(2)
	}

	trysetenv("REQUEST", req)
	trysetenv("HOSTNAME", parsed.Hostname())
	trychainload(0)
}

func trychainload(n_ownargs int) {
	if len(os.Args) < n_ownargs + 2 {
		fmt.Fprintln(os.Stderr, texts.errprefixChainload + "no chainload target given")
		io.WriteString(os.Stdout, texts.msgPossiblyMisconfigured)
		os.Exit(1)
	}
	exename := os.Args[n_ownargs+1]
	exepath, err := exec.LookPath(exename)
	if err != nil {
		fmt.Fprintln(os.Stderr, texts.errprefixChainload + "chainload target not found")
		io.WriteString(os.Stdout, texts.msgPossiblyMisconfigured)
		os.Exit(1)
	}
	err = syscall.Exec(exepath, os.Args[n_ownargs+1:], os.Environ())
	if err != nil {
		fmt.Fprintln(os.Stderr, texts.errprefixChainload + err.Error())
		io.WriteString(os.Stdout, texts.msgPossiblyMisconfigured)
		os.Exit(1)
	}
}

func trysetenv(key, value string) {
	err := os.Setenv(key, value)
	if err != nil {
		fmt.Fprintln(os.Stderr, texts.errprefixSetenv + err.Error())
		io.WriteString(os.Stdout, texts.msgPossiblyMisconfigured)
		os.Exit(1)
	}
}

func reap() {
	time.Sleep(10 * time.Second)
	io.WriteString(os.Stdout, texts.msgClientTimeout)
	os.Exit(2)
}

type textset struct {
	msgOver1026 string
	msgParseError string
	msgPossiblyMisconfigured string
	msgClientTimeout string

	errprefixSetenv string
	errprefixChainload string
}
